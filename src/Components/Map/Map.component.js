/* eslint-disable no-var */
import React, { Component } from 'react';
import L from 'leaflet';
import 'leaflet-routing-machine';
import './Map.style.scss';
import 'leaflet/dist/leaflet.css';

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            points: []
        };
    }

    componentDidMount() {
        this.map = L.map('map', {
            center: [56.96953, 24.37367],
            zoom: 16,
            layers: [
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution:
              '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                })
            ]
        });

        this.map.on('click', (e) => {
            const { points } = this.state;
            if (!points.length) return;
            const { latlng } = e;
            const nearest = points.filter(({ lat, lng }) => {
                const point = L.latLng([lat, lng]);
                return (point.distanceTo(latlng) < 50);
            });

            if (!nearest.length) return;

            const fNearest = this.removeDuplicates(nearest, 'route');
            console.log(fNearest);
            var text = 'Route: <br />';
            fNearest.forEach(({ route }) => {
                text += route;
                text += '<br />';
            });
            L.popup()
                .setLatLng(latlng)
                .setContent(`<p>${text}</p>`)
                .openOn(this.map);
        });

        const url = 'http://localhost:8000/graphql';
        const query = JSON.stringify({
            query: `
            query {
              trips(first: 100) {
                data {
                  trip_headsign
                  route {
                    arrival_time
                    stops {
                      stop_lat
                      stop_lon
                      stop_name
                      stop_url
                    }
                  }
                }
              }
            }
    `
        });

        this.setLoading(true);
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: query
        })
            .then((res) => res.json())
            .then(({ data: { trips: { data } } }) => {
                const routeWaypoints = data.reduce((acc, { route, trip_headsign }) => {
                    const waypoints = route.reduce((acc, { stops }) => {
                        const { stop_lat, stop_lon } = stops[0];
                        acc.push(L.latLng(stop_lat, stop_lon));
                        return acc;
                    }, []);
                    const info = { waypoints, trip_headsign };
                    acc.push(info);
                    return acc;
                }, []).filter(({ waypoints }) => waypoints.length < 25);

                routeWaypoints.forEach(({ waypoints, trip_headsign }) => {
                    // eslint-disable-next-line global-require
                    const rColor = require('randomcolor');
                    const color = rColor.randomColor();

                    L.Routing.control({
                        waypoints,
                        routeLine: (route) => {
                            // console.log(route);
                            const { coordinates } = route;

                            // console.log(coordinates);
                            coordinates.forEach((coord) => {
                                // eslint-disable-next-line no-param-reassign
                                coord.route = trip_headsign;

                                this.setState(({ points }) => {
                                    points.push(coord);

                                    return { points };
                                });
                            });

                            const line = L.Routing.line(route,
                                {
                                    styles: [{ color, opacity: 1, weight: Math.random() * 20 }]
                                });
                            return line;
                        },
                        options: { name: 'hello' },
                        autoRoute: true,

                        // eslint-disable-next-line max-len
                        // eslint-disable-next-line new-cap
                        router: new L.Routing.mapbox('pk.eyJ1Ijoic2xvcHB5bmljazMiLCJhIjoiY2sxd3Nxajc0MDB0dTNkcGM5OXhrbGdnZSJ9.hK3_6kZUGJ9QDzEYlzl3vw')
                    }).addTo(this.map);

                    // route.on('routeselected', (e) => {
                    //     console.log(e);
                    //     e.route.coordinates.forEach((system) => {
                    //         system['name'] = 'test';
                    //     });
                    // });
                });

                this.setLoading(false);
                // markers.clearLayers();
                // for (let i = 0; i < stops.length; i++) {
                //     const { stop_lon, stop_lat, stop_name } = stops[i];

                //     L.circleMarker([stop_lat, stop_lon], { readius: 20, ...circleStyle }).addTo(markers)
                //         .bindPopup(stop_name);
                // }
            });
    }

    setLoading(loading) {
        this.setState({
            loading
        });
    }

    removeDuplicates(originalArray, prop) {
        var newArray = [];
        var lookupObject = {};

        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }

        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    }

    render() {
        const { loading } = this.state;
        return (
            <>
              <div id="map" />
              <p style={ { color: 'white' } }>{ loading ? 'Loading' : null }</p>
            </>
        );
    }
}

export default Map;
